#include "Callback.hpp"
#include "Pile.hpp"

stack <float> oprnds;		//pour construire l objet on declare une stack
Pile pile1(oprnds);			//on crée un objet pile1 global que l on pourra manipuler

/************** FUNCTION BOUTON CHIFFRE ************/

void func_zero(Widget w, void *data)
{
	// On caste le pointeur de nimporte quel type en Saisie	
	Saisie *struct_calcul = (Saisie*) data;
	
	string buffer = GetStringEntry(struct_calcul->zone_affichage);
	if(isError(buffer))
		buffer = "0";
	else if(hasNoOpeUnlessMinus(buffer))
		buffer = buffer + "0";

	char s[32] = {0};

	// Création d'un char*
	strcpy (s, buffer.c_str());
	SetStringEntry(struct_calcul->zone_affichage, s);
}

void func_un(Widget w, void *data)
{
	// On caste le pointeur de nimporte quel type en Saisie	
	Saisie *struct_calcul = (Saisie*) data;
	
	string buffer = GetStringEntry(struct_calcul->zone_affichage);
	if(isError(buffer))
		buffer = "1";
	else if(hasNoOpeUnlessMinus(buffer))
		buffer = buffer + "1";

	char s[32] = {0};

	// Création d'un char*
	strcpy (s, buffer.c_str());
	SetStringEntry(struct_calcul->zone_affichage, s);
}

void func_deux(Widget w, void *data)
{
	// On caste le pointeur de nimporte quel type en Saisie	
	Saisie *struct_calcul = (Saisie*) data;
	
	string buffer = GetStringEntry(struct_calcul->zone_affichage);
	if(isError(buffer))
		buffer = "2";
	else if(hasNoOpeUnlessMinus(buffer))
		buffer = buffer + "2";

	char s[32] = {0};

	// Création d'un char*
	strcpy (s, buffer.c_str());
	SetStringEntry(struct_calcul->zone_affichage, s);
}

void func_trois(Widget w, void *data)
{
	// On caste le pointeur de nimporte quel type en Saisie	
	Saisie *struct_calcul = (Saisie*) data;
	
	string buffer = GetStringEntry(struct_calcul->zone_affichage);
	if(isError(buffer))
		buffer = "3";
	else if(hasNoOpeUnlessMinus(buffer))
		buffer = buffer + "3";

	char s[50] = {0};

	// Création d'un char*
	strcpy (s, buffer.c_str());
	SetStringEntry(struct_calcul->zone_affichage, s);
}

void func_quatre(Widget w, void *data)
{
	// On caste le pointeur de nimporte quel type en Saisie	
	Saisie *struct_calcul = (Saisie*) data;
	
	string buffer = GetStringEntry(struct_calcul->zone_affichage);
	if(isError(buffer))
		buffer = "4";
	else if(hasNoOpeUnlessMinus(buffer))
		buffer = buffer + "4";

	char s[32] = {0};

	// Création d'un char*
	strcpy (s, buffer.c_str());
	SetStringEntry(struct_calcul->zone_affichage, s);
}

void func_cinq(Widget w, void *data)
{
	// On caste le pointeur de nimporte quel type en Saisie	
	Saisie *struct_calcul = (Saisie*) data;
	
	string buffer = GetStringEntry(struct_calcul->zone_affichage);
	if(buffer.compare("ERROR") == 0
	   || buffer.compare("inf") == 0)
		buffer = "5";
	else if(hasNoOpeUnlessMinus(buffer))
		buffer = buffer + "5";

	char s[32] = {0};

	// Création d'un char*
	strcpy (s, buffer.c_str());
	SetStringEntry(struct_calcul->zone_affichage, s);
}

void func_six(Widget w, void *data)
{
	// On caste le pointeur de nimporte quel type en Saisie	
	Saisie *struct_calcul = (Saisie*) data;
	
	string buffer = GetStringEntry(struct_calcul->zone_affichage);
	if(isError(buffer))
		buffer = "6";
	else if(hasNoOpeUnlessMinus(buffer))
		buffer = buffer + "6";

	char s[32] = {0};

	// Création d'un char*
	strcpy (s, buffer.c_str());
	SetStringEntry(struct_calcul->zone_affichage, s);
}

void func_sept(Widget w, void *data)
{
	// On caste le pointeur de nimporte quel type en Saisie	
	Saisie *struct_calcul = (Saisie*) data;
	
	string buffer = GetStringEntry(struct_calcul->zone_affichage);
	if(isError(buffer))
		buffer = "7";
	else if(hasNoOpeUnlessMinus(buffer))
		buffer = buffer + "7";

	char s[32] = {0};

	// Création d'un char*
	strcpy (s, buffer.c_str());
	SetStringEntry(struct_calcul->zone_affichage, s);
}

void func_huit(Widget w, void *data)
{
	// On caste le pointeur de nimporte quel type en Saisie	
	Saisie *struct_calcul = (Saisie*) data;
	
	string buffer = GetStringEntry(struct_calcul->zone_affichage);
	if(isError(buffer))
		buffer = "8";
	else if(hasNoOpeUnlessMinus(buffer))
		buffer = buffer + "8";

	char s[32] = {0};

	// Création d'un char*
	strcpy (s, buffer.c_str());
	SetStringEntry(struct_calcul->zone_affichage, s);
}

void func_neuf(Widget w, void *data)
{
	// On caste le pointeur de nimporte quel type en Saisie	
	Saisie *struct_calcul = (Saisie*) data;
	
	string buffer = GetStringEntry(struct_calcul->zone_affichage);
	if(isError(buffer))
		buffer = "9";
	else if(hasNoOpeUnlessMinus(buffer))
		buffer = buffer + "9";

	char s[32] = {0};

	// Création d'un char*
	strcpy (s, buffer.c_str());
	SetStringEntry(struct_calcul->zone_affichage, s);
}

void func_virgule(Widget w, void *data)
{
	// On caste le pointeur de nimporte quel type en Saisie	
	Saisie *struct_calcul = (Saisie*) data;

	string buffer = GetStringEntry(struct_calcul->zone_affichage);

	size_t found = buffer.find(".");

	if(found == string::npos)
	{
		if(isError(buffer))
			buffer = "0.";
		else if(buffer.compare("") == 0
			|| buffer.compare("-") == 0)
			buffer = buffer + "0.";
		else if(hasNoOpeUnlessMinus(buffer))
			buffer = buffer + ".";
	}		

	char s[32] = {0};

	// Création d'un char*
	strcpy (s, buffer.c_str());
	SetStringEntry(struct_calcul->zone_affichage, s);
}

/************* FIN BOUTON CHIFFRE *************/

/************ FONCTION BOUTON OPERATOIRE ************/

void func_multiplie(Widget w, void *data)
{
	// On caste le pointeur de nimporte quel type en Saisie	
	Saisie *struct_calcul = (Saisie*) data;
	
	string buffer = GetStringEntry(struct_calcul->zone_affichage);
	if(isError(buffer))
		buffer = "*";
	else if(isOperator(buffer))
		buffer = "*";
	else if(buffer.compare("") == 0)
		buffer = "*";

	char s[32] = {0};

	// Création d'un char*
	strcpy (s, buffer.c_str());
	SetStringEntry(struct_calcul->zone_affichage, s);
}

void func_divise(Widget w, void *data)
{
	// On caste le pointeur de nimporte quel type en Saisie	
	Saisie *struct_calcul = (Saisie*) data;
	
	string buffer = GetStringEntry(struct_calcul->zone_affichage);
	if(isError(buffer))
		buffer = "/";
	else if(isOperator(buffer))
		buffer = "/";
	else if(buffer.compare("") == 0)
		buffer = "/";

	char s[32] = {0};

	// Création d'un char*
	strcpy (s, buffer.c_str());
	SetStringEntry(struct_calcul->zone_affichage, s);
}

void func_moins(Widget w, void *data)
{
	// On caste le pointeur de nimporte quel type en Saisie	
	Saisie *struct_calcul = (Saisie*) data;
	
	string buffer = GetStringEntry(struct_calcul->zone_affichage);
	if(isError(buffer))
		buffer = "-";
	else if(isOperator(buffer))
		buffer = "-";
	else if(buffer.compare("") == 0)
		buffer = "-";

	char s[32] = {0};

	// Création d'un char*
	strcpy (s, buffer.c_str());
	SetStringEntry(struct_calcul->zone_affichage, s);
}

void func_plus(Widget w, void *data)
{
	// On caste le pointeur de nimporte quel type en Saisie	
	Saisie *struct_calcul = (Saisie*) data;
	
	string buffer = GetStringEntry(struct_calcul->zone_affichage);
	if(isError(buffer))
		buffer = "+";
	else if(isOperator(buffer))
		buffer = "+";
	else if(buffer.compare("") == 0)
		buffer = "+";

	char s[32] = {0};

	// Création d'un char*
	strcpy (s, buffer.c_str());
	SetStringEntry(struct_calcul->zone_affichage, s);
}

void func_puissance(Widget w, void *data)
{
	// On caste le pointeur de nimporte quel type en Saisie	
	Saisie *struct_calcul = (Saisie*) data;
	
	string buffer = GetStringEntry(struct_calcul->zone_affichage);
	if(isError(buffer))
		buffer = "^";
	else if(isOperator(buffer))
		buffer = "^";
	else if(buffer.compare("") == 0)
		buffer = "^";

	char s[32] = {0};

	// Création d'un char*
	strcpy (s, buffer.c_str());
	SetStringEntry(struct_calcul->zone_affichage, s);
}


void func_pourcent(Widget w, void *data)
{
	// On caste le pointeur de nimporte quel type en Saisie	
	Saisie *struct_calcul = (Saisie*) data;
	
	string buffer = GetStringEntry(struct_calcul->zone_affichage);
	if(isError(buffer))
		buffer = "%";
	else if(isOperator(buffer))
		buffer = "%";
	else if(buffer.compare("") == 0)
		buffer = "%";

	char s[32] = {0};

	// Création d'un char*
	strcpy (s, buffer.c_str());
	SetStringEntry(struct_calcul->zone_affichage, s);
}

void func_exp(Widget w, void *data)
{
	// On caste le pointeur de nimporte quel type en Saisie	
	Saisie *struct_calcul = (Saisie*) data;
	
	string buffer = GetStringEntry(struct_calcul->zone_affichage);
	if(isError(buffer))
		buffer = "exp";
	else if(isOperator(buffer))
		buffer = "exp";
	else if(buffer.compare("") == 0)
		buffer = "exp";

	char s[32] = {0};

	// Création d'un char*
	strcpy (s, buffer.c_str());
	SetStringEntry(struct_calcul->zone_affichage, s);
}

void func_log(Widget w, void *data)
{
	// On caste le pointeur de nimporte quel type en Saisie	
	Saisie *struct_calcul = (Saisie*) data;
	
	string buffer = GetStringEntry(struct_calcul->zone_affichage);
	if(isError(buffer))
		buffer = "log";
	else if(isOperator(buffer))
		buffer = "log";
	else if(buffer.compare("") == 0)
		buffer = "log";

	char s[32] = {0};

	// Création d'un char*
	strcpy (s, buffer.c_str());
	SetStringEntry(struct_calcul->zone_affichage, s);
}


/************* FIN BOUTON OPERATOIRE **************/


/*************** BOUTON AUTRES ****************/

void func_suppr(Widget w, void *data)
{
	// On caste le pointeur de nimporte quel type en Saisie	
	Saisie *struct_calcul = (Saisie*) data;

	string buffer = GetStringEntry(struct_calcul->zone_affichage);
	buffer = buffer.substr(0, buffer.size() - 1);

	char s[32] = {0};

	// Création d'un char*
	strcpy (s, buffer.c_str());
	SetStringEntry(struct_calcul->zone_affichage, s);
}

void func_clear(Widget w, void *data)
{
	// On caste le pointeur de nimporte quel type en Saisie	
	Saisie *struct_calcul = (Saisie*) data;

	string buffer = "";		

	char s[32] = {0};

	// Création d'un char*
	strcpy (s, buffer.c_str());
	SetStringEntry(struct_calcul->zone_affichage, s);
}

void func_reset(Widget w, void *data)
{
	// On caste le pointeur de nimporte quel type en Saisie	
	
	Saisie *struct_calcul = (Saisie*) data;

	string buffer = "";		

	char s[32] = {0};

	// Création d'un char*
	strcpy (s, buffer.c_str());
	SetStringEntry(struct_calcul->zone_affichage, s);
	struct_calcul->nb_memoire=0;
	
	while(!pile1.operandes.empty())
	{
		pile1.operandes.pop();
	}
}

void func_enter(Widget w, void *data)
{
	// On caste le pointeur de nimporte quel type en Saisie	 
	Saisie *struct_calcul = (Saisie*) data;

	// On remplit la pile1.hstring buffer = GetStringEntry(struct_calcul->zone_affichage);
	string buffer = GetStringEntry(struct_calcul->zone_affichage);
	// test les messages d erreur eventuels
	if(isOperator(buffer)
	   || isValidNumber(buffer))
	{
		if(hasNoOperator(buffer))
		{
			pile1.operandes.push(stof(buffer));
			cout << pile1.operandes.size() << " nombre" << endl;
			buffer = "";
		}
		else
		{
			buffer = pile1.func_calcul(buffer);
			if(!isError(buffer))
			{
				try
				{
					pile1.operandes.push(stof(buffer));
					if(pile1.operandes.size() == 1)
						pile1.operandes.pop();
					else
						buffer = "";
				}
				catch(const out_of_range& oor)
				{
					buffer = "OUT OF RANGE";
				}
			}
		}
	}

	char s[32] = {0};

	// Création d'un char*
	strcpy (s, buffer.c_str());
	SetStringEntry(struct_calcul->zone_affichage, s);
}

/*************** FIN BOUTON AUTRES ****************/

/**************** FONCTIONS BOUTON M ****************/

bool isValidNumberMplus(string str)
{
	bool test = false;
	
	while(!str.empty())
	{	
		if(str.back()=='0'
		   || str.back() == '1'
		   || str.back() == '2'
		   || str.back() == '3'
		   || str.back() == '4'
		   || str.back() == '5'
		   || str.back() == '6'
		   || str.back() == '7'
		   || str.back() == '8'
		   || str.back() == '9'
		   || str.back() == '.'
		   || str.back() == '-')
		{
			test = true;
			str.pop_back();
		}
		else
			return false;
	}

	return test;
}

void func_mplus(Widget w, void *data)
{
	// On caste le pointeur de nimporte quel type en Saisie	
	Saisie *struct_calcul = (Saisie*) data;

	// Création char* pour vider zone affichage
	char vide[1] = {};
	float montant;

	string buffer = GetStringEntry(struct_calcul->zone_affichage);

	// On vérifie que la donnée envoyée puisse être additionné
	if(isValidNumberMplus(buffer))
	{	
    		montant = stof(buffer);
		struct_calcul->nb_memoire = struct_calcul->nb_memoire + montant;
		cout << "Voici le montant : " << struct_calcul->nb_memoire << endl;
	}
}

void func_mc(Widget w, void *data)
{
	// On caste le pointeur de nimporte quel type en Saisie	
	Saisie *struct_calcul = (Saisie*) data;

	// Remise à 0 de la mémoire
	struct_calcul->nb_memoire = 0;

	//Vide zone d'écriture
	char vide[1] = {};
	SetStringEntry(struct_calcul->zone_affichage, vide);
}

void func_mr(Widget w, void *data)
{
	// On caste le pointeur de nimporte quel type en Saisie	
	Saisie *struct_calcul = (Saisie*) data;

	//recupère valeur double en string
	string buffer = to_string(struct_calcul->nb_memoire);
	char *cchaine;

	// Transforme un string en char pour pouvoir l'afficher
	strcpy(cchaine, buffer.c_str());

	SetStringEntry(struct_calcul->zone_affichage, cchaine);
}

/****************FIN BOUTON M ****************/

/**************** Partie Validation & Calculatoire ****************/

bool isOperator(string buffer)
{
	if(buffer.compare("*") == 0
	   || buffer.compare("+") == 0
	   || buffer.compare("-") == 0
	   || buffer.compare("/") == 0
	   || buffer.compare("^") == 0
	   || buffer.compare("%") == 0
	   || buffer.compare("exp") == 0
	   || buffer.compare("log") == 0)
		return true;
	else
		return false;
}

//Verification des Opérateurs sans tenir compte du moins pour les chiffres négatifs
bool hasNoOpeUnlessMinus(string buffer)
{
	if(buffer.compare("*") != 0
	   && buffer.compare("+") != 0
	   && buffer.compare("/") != 0
	   && buffer.compare("^") != 0
	   && buffer.compare("%") != 0
	   && buffer.compare("exp") != 0
	   && buffer.compare("log") != 0)
		return true;
	else
		return false;
}

bool hasNoOperator(string buffer)
{
	if(buffer.compare("*") != 0
	   && buffer.compare("+") != 0
	   && buffer.compare("-") != 0
	   && buffer.compare("/") != 0
	   && buffer.compare("^") != 0
	   && buffer.compare("%") != 0
	   && buffer.compare("exp") != 0
	   && buffer.compare("log") != 0)
		return true;
	else
		return false;
}

bool isError(string buffer)
{
	if(buffer.compare("ERROR") == 0
	   || buffer.compare("inf") == 0
	   || buffer.compare("OUT OF RANGE") == 0
	   || buffer.compare("NOT ENOUGH OPERANDS") == 0)
		return true;
	else
		return false;
}

bool isValidNumber(string str)
{
	bool test = false;
	
	while(!str.empty())
	{	
		if(str.back()=='0'
		   || str.back() == '1'
		   || str.back() == '2'
		   || str.back() == '3'
		   || str.back() == '4'
		   || str.back() == '5'
		   || str.back() == '6'
		   || str.back() == '7'
		   || str.back() == '8'
		   || str.back() == '9'
		   || str.back() == '.'
		   || str.back() == '-')
		{
			test = true;
			str.pop_back();
		}
		else
			return false;
	}

	return test;
}
