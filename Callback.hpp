#include <iostream>
#include <string>
#include <cstring>
#include <fstream>
#include <cstdlib>
#include <sstream>
#include "libsx.h"
#include <cmath>
#include "Pile.hpp"

using namespace std;

// Création et initialisation du constructeur de structure CurrentAmount
typedef struct Saisie
{
	float nb_memoire;
	Widget zone_affichage;
} Saisie;

// Chiffres
void func_zero(Widget w, void *data);
void func_un(Widget w, void *data);
void func_deux(Widget w, void *data);
void func_trois(Widget w, void *data);
void func_quatre(Widget w, void *data);
void func_cinq(Widget w, void *data);
void func_six(Widget w, void *data);
void func_sept(Widget w, void *data);
void func_huit(Widget w, void *data);
void func_neuf(Widget w, void *data);
void func_virgule(Widget w, void *data);

// Opérateurs
void func_multiplie(Widget w, void *data);
void func_plus(Widget w, void *data);
void func_moins(Widget w, void *data);
void func_divise(Widget w, void *data);
void func_puissance(Widget w, void *data);
void func_pourcent(Widget w, void *data);
void func_exp(Widget w, void *data);
void func_log(Widget w, void *data);

// Autres
void func_suppr(Widget w, void *data);
void func_clear(Widget w, void *data);
void func_reset(Widget w, void *data);
void func_enter(Widget w, void *data);

// Pile mémoire
void func_mplus(Widget w, void *data);
void func_mr(Widget w, void *data);
void func_mc(Widget w, void *data);

// Rajout de la stack pour la fonction calcul
bool isOperator(string buffer);
bool hasNoOperator(string buffer);
bool hasNoOpeUnlessMinus(string buffer);
bool isError(string buffer);
bool isValidNumber(string str);
//string func_calcul(string enter);
