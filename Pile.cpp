#include "Pile.hpp"

Pile::Pile ()		//constructeur de la classe pile par défaut
{
}

Pile::Pile (stack <float> oprd)		//constructeur de la classe pile
{
  this->operandes=oprd;
}


stack <float> Pile::getOperande()	//getter de l operande
{
	return this->operandes;
}


string Pile::func_calcul(string enter)	// fonction de calcul par notation polonaise postfixée empile dépile
{
	float operande1;
	float operande2;

	if(enter == "+")
	{
		if(this->operandes.size() < 2)
			return "NOT ENOUGH OPERANDS";
		else
		{
			operande2 = this->operandes.top();
			this->operandes.pop();
			operande1 = this->operandes.top();
			this->operandes.pop();
			cout << "operande1 : " << operande1 << endl;
			cout << "operande2 : " << operande2 << endl;
			cout << "resultat : " << operande1 + operande2 << endl;
			return to_string(operande1 + operande2);
		}
	}
	else if(enter == "-")
	{
		if(this->operandes.size() < 2)
			return "NOT ENOUGH OPERANDS";
		else
		{
			operande2 = this->operandes.top();
			this->operandes.pop();
			operande1 = this->operandes.top();
			this->operandes.pop();
			cout << "operande1 : " << operande1 << endl;
			cout << "operande2 : " << operande2 << endl;
			cout << "resultat : " << operande1 - operande2 << endl;
			return to_string(operande1 - operande2);
		}
	}
	else if(enter == "*")
	{
		if(this->operandes.size() < 2)
			return "NOT ENOUGH OPERANDS";
		else
		{
			operande2 = this->operandes.top();
			this->operandes.pop();
			operande1 = this->operandes.top();
			this->operandes.pop();
			cout << "operande1 : " << operande1 << endl;
			cout << "operande2 : " << operande2 << endl;
			cout << "resultat : " << operande1 * operande2 << endl;
			return to_string(operande1 * operande2);
		}
	}
	else if(enter == "/")
	{
		if(this->operandes.size() < 2)
			return "NOT ENOUGH OPERANDS";
		else
		{
			operande2 = this->operandes.top();
			this->operandes.pop();
			operande1 = this->operandes.top();
			this->operandes.pop();
			cout << "operande1 : " << operande1 << endl;
			cout << "operande2 : " << operande2 << endl;
			cout << "resultat : " << operande1 / operande2 << endl;
			return to_string(operande1 / operande2);
		}
	}
	else if(enter == "^")
	{
		if(this->operandes.size() < 2)
			return "NOT ENOUGH OPERANDS";
		else
		{
			operande2 = this->operandes.top();
			this->operandes.pop();
			operande1 = this->operandes.top();
			this->operandes.pop();
			cout << "operande1 : " << operande1 << endl;
			cout << "resultat : " << pow(operande1, operande2) << endl;
			return to_string(pow(operande1, operande2));
		}
	}
	else if(enter == "%")
	{
		if(this->operandes.size() < 1)
			return "NOT ENOUGH OPERANDS";
		else
		{
			operande1 = this->operandes.top();
			this->operandes.pop();
			cout << "operande1 : "<< operande1 << endl;
			cout << "resultat : "<< operande1 / 100 << endl;
			return to_string(operande1 / 100);
		}
	}
	else if(enter == "exp")
	{
		if(this->operandes.size() < 1)
			return "NOT ENOUGH OPERANDS";
		else
		{
			operande1 = this->operandes.top();
			this->operandes.pop();
			cout << "operande1 : "<< operande1 << endl;
			cout << "resultat : "<< exp(operande1) << endl;
			return to_string(exp(operande1));
		}
	}
	else if(enter == "log")
	{
		if(this->operandes.size() < 1)
			return "NOT ENOUGH OPERANDS";
		else
		{
			operande1 = this->operandes.top();
			this->operandes.pop();
			cout << "operande1 : "<< operande1 << endl;
			cout << "resultat : "<< log10(operande1) << endl;
			return to_string(log10(operande1));
		}
	}
	else
		return "ERROR";
}
