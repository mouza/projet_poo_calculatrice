#ifndef Pile_HPP
#define Pile_HPP

#include <iostream>
#include <string>
#include <cstring>
#include <fstream>
#include <cstdlib>
#include <sstream>
#include "libsx.h"
#include <stack>
#include <cmath>

using namespace std;

class Pile
{
	
	
	public:
		/*constructeurs*/
		Pile();
		Pile(stack <float> oprd);

		stack <float> operandes;
		string func_calcul(string);
		stack <float> getOperande();
		//setOperande(stack <float> operandes);
};

#endif