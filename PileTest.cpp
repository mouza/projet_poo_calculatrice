#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "Pile.hpp"

TEST_CASE( "Simulation du calcul 3 + 3 = 6.000000", "All" )
{	

	stack <float> oprnds;
 	Pile pile1(oprnds);	

 	string buffer = "3";
 	pile1.operandes.push(stof(buffer));		//on empile notre premier 3

 	buffer = "3";
 	pile1.operandes.push(stof(buffer));		//on empile notre second trois

 	buffer = "+";							// on rentre notre operateur
 	
	CHECK(pile1.func_calcul(buffer)=="6.000000"); 	// et c'est magique

	while(!pile1.operandes.empty())
	{
		pile1.operandes.pop();
	}

}

TEST_CASE( "Simulation du calcul (3 * 4 + 5 * 6) = 42 ", "All" )
{	

	stack <float> oprnds;
 	Pile pile1(oprnds);	

 	string buffer = "3";
 	pile1.operandes.push(stof(buffer));		//on empile notre premier 3
 	buffer = "4";
 	pile1.operandes.push(stof(buffer));		//on empile notre 4
 	buffer = "*";							// on rentre notre operateur
	buffer=pile1.func_calcul(buffer);
	CHECK(buffer=="12.000000"); 	// et c'est magique
	pile1.operandes.push(stof(buffer));		//on empile notre operande de second niveau : 12

	buffer = "5";
 	pile1.operandes.push(stof(buffer));		//on empile 5
 	buffer = "6";
 	pile1.operandes.push(stof(buffer));		//on empile 6
 	buffer = "*";							// on rentre notre operateur *
 	buffer=pile1.func_calcul(buffer);
	CHECK(buffer=="30.000000"); 			// et ça c'est magique
	pile1.operandes.push(stof(buffer));		//on empile notre operande de second niveau : 30

	buffer = "+";							// on rentre notre operateur
	/*On initialise le camion de pompier avec le code top secret*/
	CHECK(pile1.func_calcul(buffer)=="42.000000"); 	// et c'est magique

	while(!pile1.operandes.empty())
	{
		pile1.operandes.pop();
	}

}

TEST_CASE( "Simulation d'erreur , All" )
{	

	stack <float> oprnds;
 	Pile pile1(oprnds);	

 	string buffer = "5";
 	pile1.operandes.push(stof(buffer));		//on empile notre premier 3
 	buffer = "6";
 	pile1.operandes.push(stof(buffer));		//on empile notre 4
 	buffer = "7";							// on rentre notre operateur
	buffer=pile1.func_calcul(buffer);
	CHECK(buffer=="ERROR"); 	// et c'est magique

	while(!pile1.operandes.empty())
	{
		pile1.operandes.pop();
	}
	
}

TEST_CASE( "Simulation du calcul 5 6 7 * + 1 - ", "All" )
{	

	stack <float> oprnds;
 	Pile pile1(oprnds);	

 	string buffer = "5";
 	pile1.operandes.push(stof(buffer));		//on empile notre premier 3
 	buffer = "6";
 	pile1.operandes.push(stof(buffer));		//on empile notre 4
 	buffer = "7";
 	pile1.operandes.push(stof(buffer));		//on empile notre 4
 	buffer = "*";							// on rentre notre operateur
	buffer=pile1.func_calcul(buffer);
	CHECK(buffer=="42.000000"); 	// et c'est magique
	pile1.operandes.push(stof(buffer));		//on empile notre operande de second niveau : 12
	buffer = "+";							// on rentre notre operateur
	buffer=pile1.func_calcul(buffer);
	CHECK(buffer=="47.000000");
	pile1.operandes.push(stof(buffer));
	CHECK(buffer=="47.000000");
	buffer = "1";
 	pile1.operandes.push(stof(buffer));		//on empile notre 4
 	buffer = "-";							// on rentre notre operateur
	buffer=pile1.func_calcul(buffer);
	CHECK(buffer=="46.000000"); 

	while(!pile1.operandes.empty())
	{
		pile1.operandes.pop();
	}



}