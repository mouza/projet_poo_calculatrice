# README #

Projet P16 -- Programmation Orienté Objet
Création d'une calculatrice HP (postfixé) en utilisation la libraire libsx
Groupe : Herizo RAFIDISON, Guillaume PANARDIE, Benoit MODERIANO, Mouzammil ABDUL.

### Comment récupérer notre code ? ###

* Vous pouvez cloner ce répertoire à partir de son [URL](https://mouza@bitbucket.org/mouza/projet_poo_calculatrice.git) dans le répertoire de votre choix :
	
```
cd $HOME/Documents
git clone https://mouza@bitbucket.org/mouza/projet_poo_calculatrice.git

```
* Pour utiliser la calculatrice vous devez être en possession de la libraire libsx. Pour l'installer vous pouvez utiliser la commande suivante : 

```sudo apt-get install libsx-dev libsx0```

* Compiler le projet: ```make```

* Exécuter le programme: ```./zz-calc```

* Compiler les test utilisant la librairie catch.hpp : ```make test```

* Exécuter les tests: ```./zz-test```

* Nettoyer le projet:
    * effacer les .o: ```make clean```
    * effacer les .o et executable: ```make cleaner```


### Librairie libsx ###

Si vous avez besoin de documentation sur le fonctionnement de la librairie libsx après installation vous pouvez vous rendre sur cet emplacement de votre ordinateur : 
[file:///usr/share/doc/libsx-dev/html/libsx.html](file:///usr/share/doc/libsx-dev/html/libsx.html)