/**
 main.cpp
Reunion repartitons des taches creation de l interface

ABDUL Mouza
MODERIANO Ben
RAFIDISON Her
PANARDIE Guillaume

12 04 2018
*/

#include "Callback.hpp"

// Constante de la taille de l'écran de la calculatrice
#define SIZE 218

void init_display (int argc, char** argv, void* data)
{
	// Création et assemblage des widgets ici
	char un[4] = " 1 ";
	char deux[4] = " 2 ";
	char trois[4] = " 3 ";
	char quatre[4] = " 4 ";
	char cinq[4] = " 5 ";
	char six[4] = " 6 ";
	char sept[4] = " 7 ";
	char huit[4] = " 8 ";
	char neuf[4] = " 9 ";
	char zero[4] = " 0 ";
	char virgule[4] = " . ";
	char plus[4] = " + ";
	char moins[4] = " - ";
	char division[4] = " / ";
	char multiplier[4] = " * ";
	char puissance[6] = " ^  ";
	char pourcent[4] = " % ";
	char exp[5] = " e  ";
	char log[6] = " lg ";
	char cl[6] = " Cl ";
	char dl[6] = " Dl ";
	char mplus[6] = " M+ ";
	char mr[6] = " MR ";
	char mc[6] = " MC ";
	char suppr[10] = "  Suppr ";
	char enter[20] = "       Enter      ";
	char reset[15] = "   Reset  ";

	/* Zone d affichage manipulable */

	// Pointeur data casté en Saisie
	Saisie *struct_calcul = (Saisie*) data;

	// Création d'un widget zone_affichage de Saisie de string
	struct_calcul->zone_affichage = MakeStringEntry(NULL, SIZE, NULL, NULL);

	// Ligne 1	boutons
	Widget bt_sept = MakeButton(sept, func_sept, struct_calcul);	
	Widget bt_huit = MakeButton(huit, func_huit, struct_calcul);
	Widget bt_neuf = MakeButton(neuf, func_neuf, struct_calcul);
	Widget bt_div = MakeButton(division, func_divise, struct_calcul);
	Widget bt_exp = MakeButton(exp, func_exp, struct_calcul);
	Widget bt_mplus = MakeButton(mplus, func_mplus, struct_calcul);

	// Ligne 2	boutons
	Widget bt_quatre = MakeButton(quatre, func_quatre, struct_calcul);
	Widget bt_cinq = MakeButton(cinq, func_cinq, struct_calcul);
	Widget bt_six = MakeButton(six, func_six, struct_calcul);
	Widget bt_multiplier = MakeButton(multiplier, func_multiplie, struct_calcul);
	Widget bt_log = MakeButton(log, func_log, struct_calcul);
	Widget bt_mr = MakeButton(mr, func_mr, struct_calcul);

	// Ligne 3	boutons
	Widget bt_un = MakeButton(un, func_un, struct_calcul);
	Widget bt_deux = MakeButton(deux, func_deux, struct_calcul);
	Widget bt_trois = MakeButton(trois, func_trois, struct_calcul);
	Widget bt_moins = MakeButton(moins, func_moins, struct_calcul);
	Widget bt_puissance = MakeButton(puissance, func_puissance, struct_calcul);
	Widget bt_mc = MakeButton(mc, func_mc, struct_calcul);

	// Ligne 4	boutons
	Widget bt_zero = MakeButton(zero, func_zero, struct_calcul);
	Widget bt_virgule = MakeButton(virgule, func_virgule, struct_calcul);
	Widget bt_pourcent = MakeButton(pourcent, func_pourcent, struct_calcul);
	Widget bt_plus = MakeButton(plus, func_plus, struct_calcul);
	Widget bt_dl = MakeButton(dl, func_suppr, struct_calcul);
	Widget bt_cl = MakeButton(cl, func_clear, struct_calcul);
	
	// Ligne 5	boutons
	Widget bt_enter = MakeButton(enter, func_enter, struct_calcul);
	Widget bt_reset = MakeButton(reset, func_reset, struct_calcul);

	// Ligne 1	set boutons
	SetWidgetPos(bt_sept, PLACE_UNDER, struct_calcul->zone_affichage, 0, NO_CARE);
	SetWidgetPos(bt_huit, PLACE_UNDER, struct_calcul->zone_affichage, PLACE_RIGHT, bt_sept);
	SetWidgetPos(bt_neuf, PLACE_UNDER, struct_calcul->zone_affichage, PLACE_RIGHT, bt_huit);
	SetWidgetPos(bt_div, PLACE_UNDER, struct_calcul->zone_affichage, PLACE_RIGHT, bt_neuf);
	SetWidgetPos(bt_exp, PLACE_UNDER, struct_calcul->zone_affichage, PLACE_RIGHT, bt_div);
	SetWidgetPos(bt_mplus, PLACE_UNDER, struct_calcul->zone_affichage, PLACE_RIGHT, bt_exp);

	// Ligne 2	set boutons
	SetWidgetPos(bt_quatre, PLACE_UNDER, bt_sept, 0, NO_CARE);
	SetWidgetPos(bt_cinq, PLACE_UNDER, bt_huit, PLACE_RIGHT, bt_quatre);
	SetWidgetPos(bt_six, PLACE_UNDER, bt_neuf, PLACE_RIGHT, bt_cinq);
	SetWidgetPos(bt_multiplier, PLACE_UNDER, bt_div, PLACE_RIGHT, bt_six);
	SetWidgetPos(bt_log, PLACE_UNDER, bt_exp, PLACE_RIGHT, bt_multiplier);
	SetWidgetPos(bt_mr, PLACE_UNDER, bt_mplus, PLACE_RIGHT, bt_log);	

	// Ligne 3	set boutons
	SetWidgetPos(bt_un, PLACE_UNDER, bt_quatre, 0, NO_CARE);
	SetWidgetPos(bt_deux, PLACE_UNDER, bt_cinq, PLACE_RIGHT, bt_un);
	SetWidgetPos(bt_trois, PLACE_UNDER, bt_six, PLACE_RIGHT, bt_deux);
	SetWidgetPos(bt_moins, PLACE_UNDER, bt_multiplier, PLACE_RIGHT, bt_trois);
	SetWidgetPos(bt_puissance, PLACE_UNDER, bt_log, PLACE_RIGHT, bt_moins);
	SetWidgetPos(bt_mc, PLACE_UNDER, bt_mr, PLACE_RIGHT, bt_puissance);

	// Ligne 4	set boutons
	SetWidgetPos(bt_zero, PLACE_UNDER, bt_un, 0, NO_CARE);
	SetWidgetPos(bt_virgule, PLACE_UNDER, bt_deux, PLACE_RIGHT, bt_zero);
	SetWidgetPos(bt_pourcent, PLACE_UNDER, bt_trois, PLACE_RIGHT, bt_virgule);
	SetWidgetPos(bt_plus, PLACE_UNDER, bt_moins, PLACE_RIGHT, bt_pourcent);
	SetWidgetPos(bt_dl, PLACE_UNDER, bt_puissance, PLACE_RIGHT, bt_plus);
	SetWidgetPos(bt_cl, PLACE_UNDER, bt_mc, PLACE_RIGHT, bt_dl);

	// Ligne 5	set boutons
	SetWidgetPos(bt_enter, PLACE_UNDER, bt_zero, 0, NO_CARE);
	SetWidgetPos(bt_reset, PLACE_UNDER, bt_dl, PLACE_RIGHT, bt_enter);

	SetBgColor(bt_div, GetRGBColor(255, 255, 40)); //jaune
	SetBgColor(bt_dl, GetRGBColor(150, 150, 150)); 
	SetBgColor(bt_plus, GetRGBColor(255, 255, 40)); 

	SetBgColor(bt_moins, GetRGBColor(255, 255, 40)); 
	SetBgColor(bt_multiplier, GetRGBColor(255, 255, 40)); 
	SetBgColor(bt_pourcent, GetRGBColor(150, 150, 150)); 
	SetBgColor(bt_exp, GetRGBColor(150, 150, 150)); 
	SetBgColor(bt_mplus, GetRGBColor(40, 200, 40)); //vert
	SetBgColor(bt_mr, GetRGBColor(40, 200, 40)); 
	SetBgColor(bt_mc, GetRGBColor(40, 200, 40));

	SetBgColor(bt_puissance, GetRGBColor(150, 150, 150)); 
	SetBgColor(bt_virgule, GetRGBColor(150, 150, 150)); 
	SetBgColor(bt_log, GetRGBColor(150, 150, 150)); 
	SetBgColor(bt_cl, GetRGBColor(150, 150, 150));

	SetBgColor(bt_reset, GetRGBColor(150, 150, 150)); //gris
	SetBgColor(bt_enter, GetRGBColor(150, 150, 150)); 
	// Gestion couleurs

	GetStandardColors();

	// Affichage de zone_affichage
	ShowDisplay();
};


int main(int argc, char** argv)
{
	Saisie valeur;
	valeur.nb_memoire = 0;

	if (OpenDisplay(argc, argv) == 0)
	{
		cout << "Erreur: Impossible de lancer l'interface graphique" << endl;
		return 1;
	}

	init_display(argc, argv, &valeur);

	MainLoop();

	return 0;
}
