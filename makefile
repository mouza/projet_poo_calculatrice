CC = g++
CXXFLAG = -std=c++11 -lsx
EXEC = zz-calc
EXEC_TEST = zz-test

all: $(EXEC)

test: $(EXEC_TEST)

zz-calc: Callback.o main.cpp Callback.hpp Pile.hpp Pile.o
	$(CC) -o $@ $^ $(CXXFLAG)

zz-test: Pile.hpp PileTest.cpp Pile.cpp
	$(CC) -o $@ $^ $(CXXFLAG)

%.o: %.cpp
	$(CC) -c $^ $(CXXFLAG)

clean: 
	rm -f *.o

cleaner: 
	rm -f *.o
	rm -f $(EXEC)
	rm -f $(EXEC_TEST)



	
